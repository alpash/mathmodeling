﻿using System;
using System.Collections;
using System.Windows;

namespace LennardJones
{
    /// <summary>
    /// Ячейка для моделирования первой задачи Васина.
    /// </summary>
    public class Cell : IEnumerable
    {
        #region Fields
        private Particle[] p;
        private readonly double lx;
        private readonly double ly;
        private readonly double dt;
        private double t;
        private static Random randX, randY;
        private readonly double Vmax;
        private double eu, ek, seu, sek;
        private readonly double f_coef;
        private readonly double d;
        private readonly double r0;
        private int lcounts;
        #endregion

        #region Properties

        /// <summary>
        /// Время.
        /// </summary>
        public double T => t;
        /// <summary>
        /// Шаг по времени.
        /// </summary>
        public double dT => dt;

        /// <summary>
        /// Потенциальная энергия системы.
        /// </summary>
        public double U => eu / 2.0;
        /// <summary>
        /// Кинетическая энергия системы.
        /// </summary>
        public double K => ek / 2.0;
        /// <summary>
        /// Полная энергия системы.
        /// </summary>
        public double E => (eu + ek) / 2.0;

        /// <summary>
        /// Потенциальная энергия одной частицы.
        /// </summary>
        public double SingleU => seu;
        /// <summary>
        /// Кинетическая энергия одной частицы.
        /// </summary>
        public double SingleK => sek;
        /// <summary>
        /// Полная энергия одной частицы.
        /// </summary>
        public double SingleE => sek + seu;

        /// <summary>
        /// Координаты i-ой частицы.
        /// </summary>
        /// <param name="i">Индекс частицы.</param>
        /// <returns>
        /// Возвращает двумерный вектор
        /// с нужными координатами.
        /// </returns>
        public Vector this[int i] => p[i].R;

        /// <summary>
        /// Количество частиц в левой половине ячейки.
        /// </summary>
        public int LeftCount => lcounts;

        #endregion

        #region Constructors

        // статический конструктор нашей каши.
        static Cell()
        {
            randX = new Random(DateTime.Now.Millisecond);
            randY = new Random(DateTime.Now.Millisecond + 128);
        }

        /// <summary>
        /// Новая ячейка для проведения эксперимента.
        /// </summary>
        /// <param name="N">Кол-во частиц внутри.</param>
        /// <param name="dt">Дискретное время.</param>
        /// <param name="Lx">Размеры по горизонтали.</param>
        /// <param name="Ly">Размеры по вертикали.</param>
        /// <param name="m">Масса одной частицы.</param>
        /// <param name="OnGrid">Выстраивать ли по решётке.</param>
        /// <param name="D">Модуль потенциальной энергии
        /// взаимодействия при равновесии.</param>
        /// <param name="R0">Равновесное расстояние между частицами.</param>
        public Cell(int N, double dt, double Lx, double Ly, double m, bool OnGrid,
            bool IsNullV, double D = 1.654e-12, double R0 = 0.382)
        {
            try
            {
                this.dt = dt;
                lx = Lx;
                ly = Ly;
                p = new Particle[N];
                
                d = D;
                r0 = R0;

                f_coef = 12.0 * d * Math.Pow(r0, 6.0);

                Vmax = Math.Sqrt(d / m);

                double k = 2.0;

                eu = 0.0;
                ek = 0.0;

                if (OnGrid)
                {
                    // расположение по сетке
                    double r14 = r0 / 4.0;  
                    double r12 = r0 / 2.0;
                    double r32 = 3.0 * r12;


                    int n0 = N % 3 == 0 ? N / 5 : N / 3;
                    int n1 = n0 == N / 5 ? 5 : 3;
                    int a = 0;

                    for (int i = 0; i < n0; i++)
                    {
                        if (a > N) break;

                        for (int j = 0; j < n1; j++)
                        {
                            Vector v = new Vector();
                            if (!IsNullV)
                            {
                                v.X = k * (2.0 * randX.NextDouble() * Vmax - Vmax);
                                v.Y = k * (2.0 * randY.NextDouble() * Vmax - Vmax);
                            }

                            p[a++] = new Particle(v, new Vector(
                                r12 + i * r32 + 2.0 * randX.NextDouble() * r14 - r14,
                                r12 + j * r32 + 2.0 * randY.NextDouble() * r14 - r14),
                                m);
                        }
                    }
                }
                else
                {
                    // рандомное расположение

                    for (int i = 0; i < N; i++)
                    {
                        Vector v = new Vector();
                        if (!IsNullV)
                        {
                            v.X = k * (2.0 * randX.NextDouble() * Vmax - Vmax);
                            v.Y = k * (2.0 * randY.NextDouble() * Vmax - Vmax);
                        }

                        p[i] = new Particle(v, new Vector(
                            randX.NextDouble() * lx, randY.NextDouble() * ly),
                            m);
                        //for (int j = 0; j < N; j++)
                        //{
                        //    if (p[j] != null && Distance(p[i].R, p[j].R) < r0 / 2.0)
                        //    {
                        //        p[i].R = new Vector(randX.NextDouble() * lx,
                        //            randY.NextDouble() * ly);
                        //        j--;
                        //    }
                        //}
                    }
                }

                // расчёт энергии и сил взаимодействия.
                for (int i = 0; i < N; i++)
                {
                    ek += SetK(i);

                    if (i != 0) seu += SetU(0, i);

                    GetLeftCount(i);

                    for (int j = 0; j < N; j++)
                    {
                        if (i != j)
                        {
                            SetFU(i, j);
                            //SetU(i, j);
                        }
                    }
                }

                sek = SetK(0) / 2.0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка при создании ячейки!");
            }
            finally
            {
                t = 0.0;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Следующий шаг по времени.
        /// </summary>
        public void NextStep()
        {
            eu = 0.0;
            ek = 0.0;
            seu = 0.0;
            lcounts = 0;
            sek = 0.0;

            for (int i = 0; i < p.Length; i++)
            {
                p[i].R += p[i].V * dt + p[i].F * dt * dt / 2.0 / p[i].M;
                p[i].CorrectR(lx, ly);

                var f = p[i].F;
                p[i].F = new Vector();
                for (int j = 0; j < p.Length; j++)
                {
                    if (j != i)
                    {
                        SetFU(i, j);
                    }
                }
                
                p[i].V += (f + p[i].F) / 2.0 / p[i].M * dt;

                ek += SetK(i);

                if (i != 0) seu += SetU(0, i);

                GetLeftCount(i);
            }

            sek += SetK(0) / 2.0;

            t += dt;
        }

        /// <summary>
        /// Реализация метода GetEnumerator() интерфейса IEnumerable.
        /// </summary>
        /// <returns>
        /// Возвращает х**ню, которая нужна для того, чтобы
        /// можно было пропускать это дело через foreach.
        /// </returns>
        public IEnumerator GetEnumerator()
        {
            return p.GetEnumerator();
        }

        #endregion

        #region Private Methods

        // задаёт силу взаимодействия i-ой с j-ой частиц и ведёт подсчёт
        // потенциВальной энергии.
        private void SetFU(int i, int j)
        {
            double dx = p[i].R.X - p[j].R.X;
            if (Math.Abs(dx) > lx / 2.0) dx -= Math.Sign(dx) * lx;

            double dy = p[i].R.Y - p[j].R.Y;
            if (Math.Abs(dy) > ly / 2.0) dy -= Math.Sign(dy) * ly;
            
            double rik = dx * dx + dy * dy;
            
            p[i].F += f_coef *
                (Math.Pow(r0, 6.0) / Math.Pow(rik, 3.0) - 1.0)
                * (new Vector(dx, dy)) / Math.Pow(rik, 4.0);

            eu += SetU(i, j);
        }

        // энергия взаимодействия i-ой j-ой частиц.
        private double SetU(int i, int j)
        {
            double dd = Math.Pow(r0 / Distance(p[i].R, p[j].R), 6.0);
            return d * (dd * dd - 2.0 * dd);
        }
        
        // считает кинетическую энергию i-ой частицы и
        // прибавляет к значению кинетической энергии системы.
        private double SetK(int i)
        {
            return p[i].M * p[i].V * p[i].V; 
        }

        // Если i-ая частица находится в левой части ячейки,
        // увеличивает соответствующее поле на единицу.
        private void GetLeftCount(int i)
        {
            if (p[i].R.X < lx / 2.0) lcounts++;
        }

        //-----------------------------------------------------

        // считает квадрат расстояния между двумя векторами.
        private static double SqrDistance(Vector a, Vector b)
        {
            return (a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y);
        }

        // считает расстояние между двумя векторами.
        private static double Distance(Vector a, Vector b)
        {
            return Math.Sqrt(SqrDistance(a, b));
        }
        
        #endregion

    }
}
