﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace LennardJones
{
    public partial class FormGraph : Form
    {
        public List<double> U { get; set; }
        public List<double> K { get; set; }
        public List<double> E { get; set; }
        public double dT { get; set; }
        public double T { get; set; }
        public int Count { get; set; }

        public FormGraph()
        {
            InitializeComponent();
            U = new List<double>();
            K = new List<double>();
            E = new List<double>();
        }

        private void FormGraph_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < Count; i++)
            {
                chart1.Series[0].Points.AddXY(dT * i, U[i]);
                chart1.Series[1].Points.AddXY(dT * i, K[i]);
                chart1.Series[2].Points.AddXY(dT * i, E[i]);
            }
        }
    }
}
