﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LennardJones
{
    public partial class FormLeftCounts : Form
    {
        public List<int> Counts { get; set; }
        public double dT { get; set; }

        public FormLeftCounts()
        {
            InitializeComponent();

            Counts = new List<int>();
        }

        private void FormLeftCounts_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < Counts.Count; i++)
            {
                chart1.Series[0].Points.AddXY(i * dT, Counts[i]);
            }
        }
    }
}
