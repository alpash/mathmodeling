﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace LennardJones
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Cell MainCell { get; set; }
        public double Lx { get; set; }
        public double Ly { get; set; }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Bttn_Start_Click(object sender, RoutedEventArgs e)
        {
            int N = Convert.ToInt32(Tb_N.Text);
            Lx = Convert.ToDouble(Tb_Lx.Text);
            Ly = Convert.ToDouble(Tb_Ly.Text);
            double dt = Convert.ToDouble(Tb_dt.Text);
            double m = Convert.ToDouble(Tb_m.Text);
            bool on_grid = Chb_InGrid.IsChecked ?? true;
            bool is_nullv = Chb_IsNullV.IsChecked ?? false;

            MainCell = new Cell(N, dt, Lx, Ly, m, on_grid, is_nullv);

            WindowCartoon wc = new WindowCartoon()
            {
                Owner = this,
                Count = N
            };

            wc.ShowDialog();
        }

        private void Tb_N_TextChanged(object sender, TextChangedEventArgs e)
        {
            //int N = Convert.ToInt32(Tb_N.Text);
            //if (N % 5 != 0) MessageBox.Show(this, "N должна быть кратна 5-ти!", "Внимание!");
            //else
            //{
            //    Lx = 0.382 * 7.0;
            //    Ly = 0.382 * 7.0;

            //    if (IsLoaded)
            //    {
            //        Tb_Lx.Text = Lx.ToString();
            //        Tb_Ly.Text = Ly.ToString();
            //    }
            //}
        }
    }
}
