﻿using System;
using System.Windows;

namespace LennardJones
{
    /// <summary>
    /// Класс наших частиц.
    /// </summary>
    internal class Particle
    {
        #region Fields
        private Vector r, v, f;
        private readonly double m;
        #endregion

        #region Properties

        /// <summary>
        /// Координаты.
        /// </summary>
        internal Vector R { get => r; set { r.X = value.X; r.Y = value.Y; } }
        /// <summary>
        /// Равнодействуящая сил взаимодействия между частицами.
        /// </summary>
        internal Vector F { get => f; set { f.X = value.X; f.Y = value.Y; } }
        /// <summary>
        /// Скорость.
        /// </summary>
        internal Vector V { get => v; set { v.X = value.X; v.Y = value.Y; } }
        /// <summary>
        /// Масса.
        /// </summary>
        internal double M => m;

        #endregion

        #region Constructors

        /// <summary>
        /// Новая частица.
        /// </summary>
        /// <param name="V">Начальная скорость.</param>
        /// <param name="R">Начальные координаты.</param>
        /// <param name="m">Масса.</param>
        internal Particle(Vector V, Vector R, double m)
        {
            v = V;
            r = R;
            this.m = m;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Применить периодические граничные условия.
        /// </summary>
        /// <param name="Lx">Ширина ячейки.</param>
        /// <param name="Ly">Высота ячейки.</param>
        internal void CorrectR(double Lx, double Ly)
        {
            //if (r.Y > 2.0 * Ly) r.Y = Ly + 0.001;
            //else if (r.Y < -2.0 * Ly) r.Y = -0.001;
            if (r.Y > Ly) r.Y -= Ly;
            else if (r.Y < 0.0) r.Y += Ly;

            //if (r.X > 2.0 * Lx) r.X = Lx + 0.001;
            //else if (r.X < -2.0 * Lx) r.X = -0.001;
            if (r.X > Lx) r.X -= Lx;
            else if (r.X < 0.0) r.X += Lx;
        }

        #endregion
    }
}
