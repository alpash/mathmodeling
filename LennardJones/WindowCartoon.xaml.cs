﻿using System;
using System.Collections.Generic;
using System.Windows.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace LennardJones
{
    /// <summary>
    /// Логика взаимодействия для WindowCartoon.xaml
    /// </summary>
    public partial class WindowCartoon : Window
    {
        MainWindow mw;
        DispatcherTimer timer;

        List<double> lU, lK, lE;
        List<double> slU, slK, slE;
        List<int> nCount;

        public int Count { set; get; }

        int size = 15;
        int thickness = 3;
        double Xc, Yc;

        int n = 1;
        bool ok = true;

        const string pause = "Пауза";
        const string play = "Продолжить";

        public WindowCartoon()
        {
            InitializeComponent();

            lU = new List<double>();
            lK = new List<double>();
            lE = new List<double>();

            slU = new List<double>();
            slK = new List<double>();
            slE = new List<double>();

            nCount = new List<int>();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            mw = (MainWindow)Owner;

            timer = new DispatcherTimer();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = new TimeSpan(0, 0, 0, 0, 1);
            timer.Start();
            timer.IsEnabled = true;// false;\

            Xc = g.Width / mw.Lx;
            Yc = g.Height / mw.Ly;
            
            for (int i = 0; i < Count; i++)
            {
                double left = mw.MainCell[i].X * Xc;// - g.Width / 2.0;
                double top = mw.MainCell[i].Y * Yc;// - g.Height / 2.0;
                double right = g.Width - left - size;
                double bottom = 0.0;// g.Height - right - size;

                Ellipse l = new Ellipse()
                {
                    Width = size,
                    Height = size,
                    VerticalAlignment = VerticalAlignment.Top,
                    Fill = Brushes.Blue,
                    Stroke = Brushes.Black,
                    StrokeThickness = thickness,
                    Margin = new Thickness(left, top, right, bottom)
                };
                g.Children.Add(l);
            }

            lU.Add(mw.MainCell.U);
            lK.Add(mw.MainCell.K);
            lE.Add(mw.MainCell.E);

            slU.Add(mw.MainCell.SingleU);
            slK.Add(mw.MainCell.SingleK);
            slE.Add(mw.MainCell.SingleE);

            nCount.Add(mw.MainCell.LeftCount);
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            mw.MainCell.NextStep();

            int i;
            for (i = 0; i < g.Children.Count; i++)
            {
                if (g.Children[i] is Ellipse) break;                
            }
            for (int j = 0; j < Count; j++)
            {
                double left = mw.MainCell[i].X * Xc;// - g.Width / 2.0;
                double top = mw.MainCell[i].Y * Yc;// - g.Height / 2.0;
                double right = g.Width - left - size;
                double bottom = 0.0;// g.Height - right - size;

                ((Ellipse)g.Children[i++]).Margin =
                    new Thickness(left, top, right, bottom);
            }

            lU.Add(mw.MainCell.U);
            lK.Add(mw.MainCell.K);
            lE.Add(mw.MainCell.E);

            slU.Add(mw.MainCell.SingleU);
            slK.Add(mw.MainCell.SingleK);
            slE.Add(mw.MainCell.SingleE);

            nCount.Add(mw.MainCell.LeftCount);

            if (ok && ++n > 1e4)
            {
                Bttn_Play_Pause.Background = Brushes.GreenYellow;
                ok = false;
            }
        }

        private void Bttn_Graph_Click(object sender, RoutedEventArgs e)
        {
            FormGraph form = new FormGraph()
            {
                U = lU,
                K = lK,
                E = lE,
                dT = mw.MainCell.dT,
                T = mw.MainCell.T,
                Count = lU.Count
            };

            form.ShowDialog();
        }

        private void Bttn_Play_Pause_Click(object sender, RoutedEventArgs e)
        {
            switch ((string)Bttn_Play_Pause.Content)
            {
                case pause:
                    Bttn_Play_Pause.Content = play;
                    break;

                case play:
                    Bttn_Play_Pause.Content = pause;
                    break;

                default:
                    break;
            }

            timer.IsEnabled ^= true;
            Bttn_Graph.IsEnabled ^= true;
            Bttn_Graph_S.IsEnabled ^= true;
            Bttn_Graph_Counts.IsEnabled ^= true;
        }

        private void Bttn_Graph_S_Click(object sender, RoutedEventArgs e)
        {
            FormGraph form = new FormGraph()
            {
                U = slU,
                K = slK,
                E = slE,
                dT = mw.MainCell.dT,
                T = mw.MainCell.T,
                Count = slU.Count
            };

            form.ShowDialog();
        }

        private void Bttn_Graph_Counts_Click(object sender, RoutedEventArgs e)
        {
            FormLeftCounts form = new FormLeftCounts()
            {
                Counts = nCount,
                dT = mw.MainCell.dT
            };

            form.ShowDialog();
        }
    }
}
