﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonteCarlo
{
    public abstract class Experiment : IExperiment
    {
        public int L { get; set; }
        public int N { get; set; }
        public int K { get; set; }
        public GridType TypeOfGrid { get; set; }

        protected Experiment(int L, int N, int K, GridType TypeOfGrid)
        {
            this.L = L;
            this.N = N;
            this.K = K;
            this.TypeOfGrid = TypeOfGrid;
        }

        public abstract double FindDs();

        public virtual double FindSqrRn()
        {
            throw new NotImplementedException();
        }

        public abstract void NextStep();
    }
}
