﻿namespace MonteCarlo
{
    /// <summary>
    /// Тип решётки.
    /// </summary>
    public enum GridType
    {
        /// <summary>
        /// Квадратная решётка.
        /// </summary>
        SquareGrid = 0,

        /// <summary>
        /// Треугольная решётка.
        /// </summary>
        TriangleGrid = 1
    }
}
