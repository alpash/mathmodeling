﻿namespace MonteCarlo
{
    /// <summary>
    /// Интерфейс для класса эксперимента.
    /// </summary>
    public interface IExperiment
    {
        /// <summary>
        /// Длина и ширина расчётного поля (ну как в методичке).
        /// </summary>
        int L { set; get; }

        /// <summary>
        /// Количество шагов в одном испытании (тоже в методичке).
        /// </summary>
        int N { set; get; }

        /// <summary>
        /// Количество испытаний (методичка).
        /// </summary>
        int K { set; get; }

        /// <summary>
        /// Тип решётки (квадратная или треугольная).
        /// </summary>
        GridType TypeOfGrid { set; get; }

        /// <summary>
        /// Следующий шаг.
        /// </summary>
        /// <remarks>
        /// Сюда впихивать тот самый рандомайзер.
        /// Короче, следующий шаг для таймера.
        /// </remarks>
        void NextStep();

        /// <summary>
        /// Найти коэффициент самодиффузии частицы.
        /// </summary>
        /// <returns>
        /// Возвращает тот самый Ds из методички
        /// (по той же формуле).
		/// </returns>
        double FindDs();

        /// <summary>
        /// Средний квадрат полного смещения частицы.
        /// </summary>
        /// <returns>
        /// Возвращает число Rn^2, которое по формуле 3.2
        /// из методички (стр.30).
        /// </returns>
        double FindSqrRn();
    }
}
