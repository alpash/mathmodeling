﻿using System.Drawing;
using System.Collections.Generic;

namespace MonteCarlo
{
    /// <summary>
    /// Интерфейс класса эксперимента для пунктов 3-4.
    /// </summary>
    /// <remarks>
    /// Реализовать в отдельном классе MultyExperiment.cs
    /// </remarks>
    public interface IMultyExperiment : IExperiment
    {
        /// <summary>
        /// Количество частиц.
        /// </summary>
        int M { set; get; }

        /// <summary>
        /// Концентрация частиц.
        /// </summary>
        double C { set; get; }

        /// <summary>
        /// Частички.
        /// </summary>
        List<PointF> Particles { set; get; }
    }
}
