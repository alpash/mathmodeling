﻿using System.Drawing;

namespace MonteCarlo
{
    /// <summary>
    /// Интерфейс класса эксперимента для пунктов 1-2.
    /// </summary>
    /// <remarks>
    /// Реализовать в отдельном классе SoloExperiment.cs
    /// </remarks>
    public interface ISoloExperiment : IExperiment
    {
        /// <summary>
        /// Частичка.
        /// </summary>
        PointF Particle { set; get; }

        /// <summary>
        /// Найти среднеквадратичное смещение частицы.
        /// </summary>
        /// <returns>
        /// Возвращает число Rn, которое по формуле 3.2
        /// из методички (стр.30).
        /// По сути просто впихни сюда квадратный корень
        /// из FindSqrRn(), и всё.
        /// </returns>
        double FindRn();

        /// <summary>
        /// Показатель степени.
        /// </summary>
        /// <returns>
        /// Возвращает степень U из формулы, где
        /// Rn ~ a*N^U (на тай же странице).
        /// </returns>
        double GetU();
    }
}
