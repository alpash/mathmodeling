﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MonteCarlo
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private GridType grid_type;
        private IExperiment experiment;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void CmbBx_Type_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (IsInitialized) SelectType();
        }

        private void CmbBx_Expr_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (IsInitialized) SelectedExpr();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SelectType();
        }

        private void Bttn_Start_Click(object sender, RoutedEventArgs e)
        {
            int N = int.Parse(Tb_N.Text);
            int L = int.Parse(Tb_L.Text);
            int K = int.Parse(Tb_K.Text);
            
            if (CmbBx_Expr.SelectedIndex == 1)
            {
                int M = int.Parse(Tb_M.Text);
                double C = double.Parse(Tb_C.Text);

                experiment = new MultyExperiment(L, N, K, M, C, grid_type);
            }
            else
            {
                experiment = new SoloExperiment(L, N, K, grid_type);
            }

            FormExperiment form = new FormExperiment(experiment);
            form.ShowDialog();
        }

        private void SelectType()
        {
            int tp = CmbBx_Type.SelectedIndex;
            if (tp < 0 || tp > 1) tp = 0;
            grid_type = (GridType)tp;
        }

        private void SelectedExpr()
        {
            int si = CmbBx_Expr.SelectedIndex;
            if (si < 0 || si > 1) si = 0;

            switch (si)
            {
                case 0:
                    Tb_C.IsEnabled = false;
                    Tb_M.IsEnabled = false;
                    break;
                case 1:
                    Tb_C.IsEnabled = true;
                    Tb_M.IsEnabled = true;
                    break;
                default:
                    break;
            }
        }
    }
}
