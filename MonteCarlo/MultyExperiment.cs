﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace MonteCarlo
{
    public class MultyExperiment : Experiment, IMultyExperiment
    {
        public int M { get; set; }
        public double C { get; set; }
        public List<PointF> Particles { set; get; }

        public MultyExperiment(int L, int N, int K, int M, double C,
            GridType TypeOfGrid) : base(L, N, K, TypeOfGrid)
        {
            this.C = C;
            this.M = M;
            Particles = new List<PointF>(M);
        }

        public override double FindDs()
        {
            throw new NotImplementedException();
        }

        public override double FindSqrRn()
        {
            return base.FindSqrRn();
        }

        public override void NextStep()
        {
            throw new NotImplementedException();
        }
    }
}
