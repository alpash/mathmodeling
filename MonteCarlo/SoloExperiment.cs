﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace MonteCarlo
{
    public class SoloExperiment : Experiment, ISoloExperiment
    {
        public PointF Particle { set; get; }

        public SoloExperiment(int L, int N, int K, GridType TypeOfGrid)
            : base(L, N, K, TypeOfGrid)
        {
        }

        public override double FindDs()
        {
            throw new NotImplementedException();
        }

        public double FindRn()
        {
            throw new NotImplementedException();
        }

        public override double FindSqrRn()
        {
            return base.FindSqrRn();
        }

        public double GetU()
        {
            throw new NotImplementedException();
        }

        public override void NextStep()
        {
            throw new NotImplementedException();
        }
    }
}
